import re

dd = {
    "one"   : 1,
    "two"   : 2,
    "three" : 3,
    "four"  : 4,
    "five"  : 5,
    "six"   : 6,
    "seven" : 7,
    "eight" : 8,
    "nine"  : 9
}

reg_str = f"(?=({'|'.join(dd.keys())}|[1-9]))"
print(reg_str)
regex = re.compile(reg_str)

def main():
    f = open("input.txt", "r")
    s = 0
    for line in f.readlines():
        line = line[:-1]
        digits = regex.findall(line)

        d1 = dd.get(digits[0], digits[0])
        d2 = dd.get(digits[-1], digits[-1])
        res = int(str(d1)+str(d2))
        s += res
        print(f"{line} - {res}")
    print(s)
    f.close()



main()
