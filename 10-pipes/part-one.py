import collections


def main():
    f = open("input.txt", "r")
    data = f.read()
    f.close()

    graph = get_graph(data)


def get_graph(data):
    split_data = data.split("\n")
    data = data.replace("\n", "")
    row_len = len(split_data[0])
    S_loc = data.index("S")
    s_row, s_col = int(S_loc/row_len), S_loc % row_len
    g = Graph(split_data, s_row, s_col)
    s = g.get_s_node()
    g.build_graph()

    # print(g.to_string())
    # for node, val in g.graph.items():
    #    print(node, val)

    print(g.maxdist)


class Graph:
    dir_compatibility = {
        "|": ["U", "D"],
        "-": ["L", "R"],
        "L": ["U", "R"],
        "J": ["U", "L"],
        "7": ["L", "D"],
        "F": ["R", "D"],
    }

    pipe_compatibility = {
        "U": ["|", "7", "F"],
        "D": ["|", "L", "J"],
        "R": ["-", "J", "7"],
        "L": ["-", "L", "F"],
    }

    def __init__(self, data, s_row, s_col):
        self.rows = len(data)
        self.cols = len(data[0])
        self.data = data
        self.nodes = {}
        self.s_row = s_row
        self.s_col = s_col
        self.s_node = "S"
        self.graph = {}
        self.maxdist = 0

    def build_graph(self):
        q = collections.deque()
        q.append(((self.s_row, self.s_col), 0))
        dist = 0
        while len(q) > 0:
            node, dist = q.pop()
            val = (self.get_node(*node), dist)
            self.graph[node] = val
            connected_nodes = self.get_connected_nodes(*node)
            for n in connected_nodes:
                if n != node and n not in self.graph:
                    q.appendleft((n, dist+1))
                    self.maxdist = dist+1

    def to_string(self):
        mat = [["." for col in range(self.cols)] for row in range(self.rows)]
        for node, val in self.graph.items():
            row, col = node
            pipe = val[0]
            mat[row][col] = pipe
        mat_string = ""
        for row in mat:
            mat_string += "".join(row) + "\n"
        return mat_string

    def get_node(self, row, col):
        if (row, col) == (self.s_row, self.s_col):
            return self.s_node
        else:
            return self.data[row][col]

    def get_dir_coords(self, row, col, dir):
        if dir == "U":
            return (row-1, col)
        if dir == "D":
            return (row+1, col)
        if dir == "L":
            return (row, col-1)
        if dir == "R":
            return (row, col+1)

    def get_node_in_dir(self, row, col, dir):
        i, j = self.get_dir_coords(row, col, dir)
        return self.get_node(i, j)

    def get_connected_nodes(self, row, col):
        curr_node = self.get_node(row, col)
        dirs = self.dir_compatibility[curr_node]
        coords = []
        for dir in dirs:
            dir_coords = self.get_dir_coords(row, col, dir)
            coords.append(dir_coords)
        return coords

    def get_surrounding_coords(self, row, col):
        coords = {"U": None, "D": None, "L": None, "R": None}
        if row > 0:
            coords["U"] = ((row-1, col))
        if row < self.rows-1:
            coords["D"] = ((row+1, col))
        if col > 0:
            coords["L"] = ((row, col-1))
        if col < self.cols-1:
            coords["R"] = ((row, col+1))
        return coords

    def get_surrounding_nodes(self, row, col):
        coords = self.get_surrounding_coords(row, col)
        nodes = {}
        for dir, coord in coords.items():
            nodes[dir] = coord
        return nodes

    def get_s_node(self):
        for pipe, dirs in self.dir_compatibility.items():
            counter = 0
            for dir in dirs:
                node = self.get_node_in_dir(self.s_row, self.s_col, dir)
                if node in self.pipe_compatibility[dir]:
                    counter += 1
                if counter == 2:
                    self.s_node = pipe
                    return pipe


main()

"""
"""
