import numpy as np
import itertools


def print_matrix(matrix):
    for row in matrix:
        print(" ".join(map(str, row)))


def parse_input(raw_data):
    raw_data = raw_data.replace("#", "1").replace(".", "0")
    data = []
    for row in raw_data.split("\n")[:-1]:
        data.append(list(map(int, row)))
    return np.array(data)


def find_zero_rows_cols(data):
    cols = np.argwhere(~np.any(data != 0, axis=0))
    rows = np.argwhere(~np.any(data != 0, axis=1))

    return rows, cols


def expand(data, rows, cols):

    zeros = np.zeros(len(data[0]))
    for i, r_i in enumerate(rows):
        data = np.insert(data, r_i+i, zeros, axis=0)

    zeros = np.zeros((len(data), 1))
    for j, c_i in enumerate(cols):
        data = np.insert(data, c_i+j, zeros, axis=1)

    return data


def enum(data):
    id = 1
    print(data[7, 12])
    res = {}
    for coord in np.argwhere(data):
        i, j = coord
        res[id] = (i, j)
        data[i, j] = id
        id += 1
    return data, res


def get_length(c1, c2):
    x1, y1 = c1
    x2, y2 = c2
    return abs(y2-y1) + abs(x2-x1)


def get_shortest_lenghts(dic):
    s = 0
    for id1, id2 in itertools.combinations(dic, 2):
        s += get_length(dic[id1], dic[id2])
    return s


def main():
    f = open("input.txt", "r")
    raw_data = f.read()
    f.close
    data = parse_input(raw_data)
    zero_rows, zero_cols = find_zero_rows_cols(data)
    data = expand(data, zero_rows, zero_cols)
    data, dic = enum(data)
    res = get_shortest_lenghts(dic)
    print(res)

    #print_matrix(data)


main()
