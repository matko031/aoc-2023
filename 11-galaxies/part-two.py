import numpy as np
import itertools


def print_matrix(matrix):
    for row in matrix:
        print(" ".join(map(str, row)))


def parse_input(raw_data):
    raw_data = raw_data.replace("#", "1").replace(".", "0")
    data = []
    for row in raw_data.split("\n")[:-1]:
        data.append(list(map(int, row)))
    return np.array(data)


def find_zero_rows_cols(data):
    cols = np.argwhere(~np.any(data != 0, axis=0))
    rows = np.argwhere(~np.any(data != 0, axis=1))

    return rows, cols


def expand(dic, zero_rows, zero_cols):
    coords = sorted(list(dic.keys()))
    new_coords = []
    exp_dist = 1000000
    #exp_dist = 10

    coord_i = 0
    row_i = 0
    row_modifier = 0

    print(np.transpose(zero_rows))
    print(np.transpose(zero_cols))
    print("")

    while coord_i < len(coords) and row_i <= len(zero_rows):

        if row_i < len(zero_rows):
            row = zero_rows[row_i]

        if coord_i < len(coords):
            coord = coords[coord_i]

        if coord[0] > row and row_i < len(zero_rows):
            row_modifier += exp_dist-1
            row_i += 1
        elif coord[0] < row or row_i == len(zero_rows):
            coord_i += 1
            new_coords.append((coord[0]+row_modifier, coord[1]))


    coord_i = 0
    col_i = 0
    col_modifier = 0
    coords = new_coords
    f_new_coords = new_coords.copy()
    new_coords = []
    zero_cols = np.transpose(zero_cols)[0]
    coords.sort(key=lambda x: x[1])

    while coord_i < len(coords) and col_i <= len(zero_cols):

        if col_i < len(zero_cols):
            col = zero_cols[col_i]

        if coord_i < len(coords):
            coord = coords[coord_i]

        if coord[1] > col and col_i < len(zero_cols):
            col_modifier += exp_dist-1
            col_i += 1
        elif coord[1] < col or col_i == len(zero_cols):
            coord_i += 1
            new_coords.append((coord[0], coord[1]+col_modifier))

    print(list(dic.keys()))
    print(f_new_coords)
    print(new_coords)
    return new_coords


def enum(data):
    id = 1
    res = {}
    for coord in np.argwhere(data):
        i, j = coord
        res[(i, j)] = id
        id += 1
    return data, res


def get_length(c1, c2):
    x1, y1 = c1
    x2, y2 = c2
    return abs(y2-y1) + abs(x2-x1)


def get_shortest_lenghts(dic):
    s = 0
    for c1, c2 in itertools.combinations(dic, 2):
        s += get_length(c1, c2)
    return s


def main():
    #f = open("test.txt", "r")
    f = open("input.txt", "r")
    raw_data = f.read()
    f.close
    data = parse_input(raw_data)
    zero_rows, zero_cols = find_zero_rows_cols(data)
    data, dic = enum(data)
    dic = expand(dic, zero_rows, zero_cols)
    res = get_shortest_lenghts(dic)
    print(res)

    # print_matrix(data)


main()
