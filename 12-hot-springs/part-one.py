def parse_line(line):
    springs, counts = line.strip().split(" ")
    counts = list(map(int, counts.strip().split(",")))
    springs = springs.strip().split()
    return springs, counts


line = "#.#.### 1,1,3"

s, c = parse_line(line)
print(s, c)
