limit = {"red": 12, "green": 13, "blue": 14}


def parse_id(id_string):
   return int(id_string.split(" ")[1])

def parse_set(set_string):
    #st = {"red": 0, "blue": 0, "green": 0}
    cubes = set_string.split(",")
    for el in cubes:
        num, col = el.strip().split(" ")
        if int(num) > limit[col]:
            return False
    return True

def parse_game(game_string):
    sets_data = game_string.split(";")

    for set_string in sets_data:
        if not parse_set(set_string):
            return False

    return True

def parse_line(line):
    id_string, data_string = line.split(":")
    id = parse_id(id_string)
    data = parse_data(data_string)


def main():
    f = open("input.txt", "r")
    s=0
    for line in f.readlines():
        id_string, game_string = line[:-1].split(":")
        id = parse_id(id_string)
        g = parse_game(game_string)
        print(id, g)
        if g:
            s += id
    f.close()
    print(s)


main()
