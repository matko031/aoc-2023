limit = {"red": 12, "green": 13, "blue": 14}


def parse_id(id_string):
   return int(id_string.split(" ")[1])

def parse_set(set_string):
    st = {"red": 0, "blue": 0, "green": 0}
    cubes = set_string.split(",")
    for el in cubes:
        num, col = el.strip().split(" ")
        st[col] = int(num)
    return st

def parse_game(game_string):
    sets_data = game_string.split(";")

    game = []

    for set_string in sets_data:
        game.append(parse_set(set_string))
    return game

def get_power(num_max):
    r = 1
    for n in num_max.values():
        r *= n
    return r


def calc_game_min(game):
    num_max = {"red": 0, "green": 0, "blue": 0}
    for st in game:
        for col, num in st.items():
            if num > num_max[col]:
                num_max[col] = num
    return num_max

def calc_game_power(game):
    return get_power( calc_game_min(game) )


def main():
    f = open("input.txt", "r")
    s=0
    for line in f.readlines():
        id_string, game_string = line[:-1].split(":")
        game = parse_game(game_string)
        s += calc_game_power(game)
    f.close()
    print(s)


main()
