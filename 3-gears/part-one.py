import re

def main():
    f = open("input.txt", "r")
    data = f.read()
    print(data)
    matrix = parse_matrix(data)
    f.close()
    process_matrix(matrix)


def parse_matrix(matrix_string):
    matrix = []
    for row in matrix_string.strip().split("\n"):
        matrix.append(row)
        get_numbers_in_row(row)
    return matrix

def process_matrix(matrix):
    s = 0
    for i, row in enumerate(matrix):
        numbers = get_numbers_in_row(row)
        for num_start, num_end in numbers:
            if is_part_number(matrix, i, num_start, num_end): 
                s += get_number(matrix, i, num_start, num_end)
    print (s)

def get_numbers_in_row(row):
    regex = re.compile("\d+")
    results = [(m.start(), m.end()) for m in regex.finditer(row)]
    return results

def get_number(matrix, row, start, end):
    return int( "".join(matrix[row][start:end]) )

def is_part_number(matrix, row, start, end):
    chars = get_surrounding_chars(matrix, row, start, end)
    return any([c != '.' for c in chars])

"""
    `end` is the index of the first character after match
    467..114..
    [(0, 3), (5, 8)]
"""
def get_surrounding_chars(matrix, row, start, end):
    chars = []
    row_len = len(matrix[row])
    if row > 0:
        chars.extend(matrix[row-1][start:end])
        if start > 0:
            chars.append(matrix[row-1][start-1])
        if end < row_len-1:
            chars.append(matrix[row-1][end])

    if start > 0:
        chars.append(matrix[row][start-1])

    if end < row_len:
        chars.append(matrix[row][end])

    if row < len(matrix)-1:
        chars.extend(matrix[row+1][start:end])
        if start > 0:
            chars.append(matrix[row+1][start-1])
        if end < row_len:
            chars.append(matrix[row+1][end])
    return "".join(chars)


main()
