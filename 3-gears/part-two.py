import re

def main():
    f = open("input.txt", "r")
    data = f.read()
    matrix = parse_matrix(data)
    f.close()
    process_matrix(matrix)


def parse_matrix(matrix_string):
    matrix = []
    for row in matrix_string.strip().split("\n"):
        matrix.append(row)
    return matrix

def process_matrix(matrix): 
    s = 0
    for i, row in enumerate(matrix):
        stars = get_stars_in_row(row)
        for star_index in stars:
            nums = get_surrounding_numbers(matrix, i, star_index)
            if len(nums)==2:
                n1 = get_number(matrix, nums[0][0], nums[0][1], nums[0][2])
                n2 = get_number(matrix, nums[1][0], nums[1][1], nums[1][2])
                s += n1*n2
    print (s)


def get_surrounding_numbers(matrix, row_index, index):
    numbers = []
    if row_index > 0:
        nums_tmp = get_numbers_in_row(matrix, row_index-1)
        for num_start, num_end in nums_tmp:
            if number_around(matrix, row_index, index, row_index-1, num_start, num_end ):
                numbers.append((row_index-1, num_start, num_end))

    if row_index < len(matrix)-1:
        nums_tmp = get_numbers_in_row(matrix, row_index+1)
        for num_start, num_end in nums_tmp:
            if number_around(matrix, row_index, index, row_index+1, num_start, num_end ):
                numbers.append((row_index+1, num_start, num_end))

    nums_tmp = get_numbers_in_row(matrix, row_index)
    for num_start, num_end in nums_tmp:
        if number_around(matrix, row_index, index, row_index, num_start, num_end ):
            numbers.append((row_index, num_start, num_end))

    return numbers





def get_numbers_in_row(matrix, row_index):
    row = matrix[row_index]
    regex = re.compile("\d+")
    results = [(m.start(), m.end()) for m in regex.finditer(row)]
    return results

def get_stars_in_row(row):
    regex = re.compile("\*")
    results = [m.start() for m in regex.finditer(row)]
    return results

def get_number(matrix, row_index, start, end):
    return int( "".join(matrix[row_index][start:end]) )

def is_part_number(matrix, row_index, start, end):
    chars = get_surrounding_chars(matrix, row_index, start, end)
    return any([c != '.' for c in chars])

"""
    `end` is the index of the first character after match
    467..114..
    [(0, 3), (5, 8)]
"""
def get_surrounding_chars(matrix, row_index, start, end):
    chars = []
    row_len = len(matrix[row_index])
    if row_index > 0:
        chars.extend(matrix[row_index-1][start:end])
        if start > 0:
            chars.append(matrix[row_index-1][start-1])
        if end < row_len-1:
            chars.append(matrix[row_index-1][end])

    if start > 0:
        chars.append(matrix[row_index][start-1])

    if end < row_len:
        chars.append(matrix[row_index][end])

    if row_index < len(matrix)-1:
        chars.extend(matrix[row_index+1][start:end])
        if start > 0:
            chars.append(matrix[row_index+1][start-1])
        if end < row_len:
            chars.append(matrix[row_index+1][end])
    return "".join(chars)

def number_around(matrix, star_row_index, star_index, num_row_index, num_start, num_end):
    if  num_start < star_index-1 and num_end-1 >= star_index-1 or \
        star_index-1 <= num_start <= star_index+1:
        return True


main()
