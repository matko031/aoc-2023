import re

def main():
    f = open("input.txt", "r")
    s = 0
    for line in f.readlines():
        winning_numbers, your_numbers = parse_cards(line[:-1])
        matches = get_number_matches(winning_numbers, your_numbers)
        points = get_points (matches)
        #print(f"{matches}-{points}")
        s += points
    f.close()
    print(s)


def parse_cards(line):
    left, your_numbers_string = line.strip().split("|")
    card_id, winning_numbers_string = left.strip().split(":")
    winning_numbers = parse_numbers(winning_numbers_string)
    your_numbers    = parse_numbers(your_numbers_string)
    return winning_numbers, your_numbers

def parse_numbers(numbers_string):
    numbers = [ num for num in numbers_string.strip().split(" ") if not num.isspace() and num != "" ]
    return list(map(int, numbers))

def get_number_matches(winning_numbers, your_numbers):
    matches = 0
    for num in your_numbers:
        if num in winning_numbers:
            matches += 1
    return matches

def get_points(num_matches):
    if num_matches > 0:
        return 2**(num_matches-1)
    return 0


main()
