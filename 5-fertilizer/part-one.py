import pprint 

line_id = 0
f = open("input.txt", "r")
data = f.read().split("\n")
f.close()

class mapping:

    def __init__(self, range_string):
        self.dest_start, self.source_start, self.length = list(map( int, range_string.strip().split(" ") ))
        self.source_end = self.source_start + self.length-1

    def __str__(self):
        return f"{self.dest_start}-{self.source_start}-{self.length}"

    def __repr__(self):
        return str(self)

class Map:
    global data, line_id

    def __init__(self):
        global data, line_id
        self.mappings = []
        while "map" in data[line_id] or data[line_id] == "":
            line_id += 1

        while data[line_id] != "":
            self.mappings.append( mapping(data[line_id]) )
            line_id +=1

        line_id += 1

    def get_value(self, val):
        for mapping in self.mappings:
            if mapping.source_start <= val <= mapping.source_end:
                return mapping.dest_start + val - mapping.source_start
        return val

    def __str__(self):
        return str(self.mappings)

    def __repr__(self):
        return str(self.mappings)



def parse_seeds(seeds_line):
    seeds = seeds_line.strip().split(":")[1].strip().split(" ")
    return list( map(int,  seeds) )

def get_maps():
    return {
        "sts" : Map(), "stf" : Map(), "ftw" : Map(), "wtl" : Map(),
        "ltt" : Map(), "tth" : Map(), "htl" : Map() 
    }

def get_data():
    global data, line_id
    seeds = parse_seeds(data[line_id])
    line_id += 1

    maps = get_maps()

    return seeds, maps

def map_values(values, map):
    res = []
    for v in values:
        res.append(map.get_value(v))
    return res

def main():
    global line_id, data
    s = 0
    seeds, maps = get_data()

    pp = pprint.PrettyPrinter()
    pp.pprint(maps)
    print("")

    print("seeds",seeds)
    soils = map_values(seeds, maps["sts"])
    print("soils",soils)
    fertilizers = map_values(soils, maps["stf"])
    print("fertilizers",fertilizers)
    waters = map_values(fertilizers, maps["ftw"])
    print("waters",waters)
    lights = map_values(waters, maps["wtl"])
    print("lights",lights)
    temperatures = map_values(lights, maps["ltt"])
    print("temperatures",temperatures)
    humidities = map_values(temperatures, maps["tth"])
    print("humidities",humidities)
    locations = map_values(humidities, maps["htl"])
    print("locations",locations)

    print(min(locations))




main()
