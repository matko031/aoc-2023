import pprint 

line_id = 0
f = open("input.txt", "r")
data = f.read().split("\n")
f.close()

class mapping:

    def __init__(self, range_string):
        self.dest_start, self.source_start, self.length = list(map( int, range_string.strip().split(" ") ))
        self.source_end = self.source_start + self.length-1
        self.dest_end = self.dest_start + self.length-1

    def map_value(self, val):
        if self.val_in_mapping(val):
            return self.dest_start + val - self.source_start
        return val

    def val_in_mapping(self, val):
        return self.source_start <= val <= self.source_end

    """
        Return tuple: ([unmapped_ranges], mapped_range)
        Unmapped ranges are the parts of initial range falling outside of source_range
        Mapped_range is the part of initial range falling within source_range and being converted to dest_range
    """
    def map_range(self, input_range):
        range_start, range_end = input_range
        print("map_range: ")
        print(f"range_start: {range_start}, range_end: {range_end}")
        print(f"source_start: {self.source_start}, source_end: {self.source_end}")

        if range_end < self.source_start or self.source_end < range_start :
            return ([input_range], None)

        elif range_start < self.source_start and  self.source_end < range_end:
            r1 = (range_start, self.source_start-1)
            r2 = (self.dest_start, self.dest_end)
            r3 = (self.source_end+1, range_end)
            return ( [r1, r3], r2 )

        elif range_start < self.source_start and self.source_start<= range_end <= self.source_end:
            r1 = (range_start, self.source_start-1)
            r2 = (self.dest_start, self.map_value(range_end))
            return [[r1,], r2]

        elif self.source_start <= range_start  and self.source_end < range_end:
            r1 = (self.map_value(range_start), self.dest_end)
            r2 = (self.source_end+1, range_end)
            return [[r2,], r1]

        elif self.source_start <= range_start and range_end <= self.source_end:
            r1 = (self.map_value(range_start), self.map_value(range_end))
            return [[], r1]

        print(f"range_start: {range_start}, range_end: {range_end}")
        print(f"source_start: {self.source_start}, source_end: {self.source_end}")
        raise Exception("Something went wrong with mapping")

    def __str__(self):
        return f"{self.dest_start}-{self.source_start}-{self.length}"

    def __repr__(self):
        return str(self)

class Map:
    global data, line_id

    def __init__(self):
        global data, line_id
        self.mappings = []
        while "map" in data[line_id] or data[line_id] == "":
            line_id += 1

        while data[line_id] != "":
            self.mappings.append( mapping(data[line_id]) )
            line_id +=1

        line_id += 1

    def get_value(self, val):
        for mapping in self.mappings:
            if mapping.val_in_mapping(val):
                return mapping.get_value(val)
        return val

    """
        input_range = (79,93),  mapping_range = (52,50,48)
    """

    def get_ranges(self, input_ranges):
        output_ranges = []
        for r in input_ranges:
            new_ranges = [r]
            for mapping in self.mappings:
                ranges = new_ranges
                new_ranges = []
                for rng in ranges: 
                    unmapped, mapped = mapping.map_range(rng)
                    if mapped is not None:
                        output_ranges.append(mapped)
                    new_ranges.extend(unmapped)
            output_ranges.extend(new_ranges)

        return output_ranges


    def __str__(self):
        return str(self.mappings)

    def __repr__(self):
        return str(self.mappings)

    

def parse_seeds(seeds_line):
    seeds = list(map(int, seeds_line.strip().split(":")[1].strip().split(" ") ))
    ranges = []
    for i in range(0, len(seeds), 2):
        start, length = seeds[i], seeds[i+1]
        ranges.append( (start, start+length-1) )
    return ranges


def get_maps():
    return {
        "sts" : Map(), "stf" : Map(), "ftw" : Map(), "wtl" : Map(),
        "ltt" : Map(), "tth" : Map(), "htl" : Map() 
    }

def get_data():
    global data, line_id
    seeds = parse_seeds(data[line_id])
    line_id += 1

    maps = get_maps()

    return seeds, maps


def main():
    global line_id, data
    s = 0
    seeds, maps = get_data()

    pp = pprint.PrettyPrinter()
    #pp.pprint(maps)
    #print("")

    print("seeds",seeds)
    pp.pprint(maps["sts"])

    soils =  maps["sts"].get_ranges(seeds)
    print("soils", soils)

    fertilizers =  maps["stf"].get_ranges(soils)
    print("fertilizers", fertilizers)

    waters =  maps["ftw"].get_ranges(fertilizers)
    print("waters", waters)

    light =  maps["wtl"].get_ranges(waters)
    print("light", light)

    temperature =  maps["ltt"].get_ranges(light)
    print("temperature", temperature)

    humidity =  maps["tth"].get_ranges(temperature)
    print("humidity", humidity)

    locations =  maps["htl"].get_ranges(humidity)
    print("location", locations)

    print(min(locations))



main()
