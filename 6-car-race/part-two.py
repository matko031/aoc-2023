import pprint 
import re
import math

"""
v = s/t_travel => s = v * t_travel
v = t_hold
t_travel = t_total-t_hold
s = t_hold * (t_total - t_hold)
-t_hold**2 + t_hold*t_total - s = 0
"""

def get_nb_ways(t_total, dist, t_h1, t_h2):
    res = math.ceil(t_h2) - math.floor(t_h1) - 1

    #if math.ceil(t_h1) == t_h1:
        #res += 1

    #if math.floor(t_h2) == t_h2:
        #res -= 1

    return res

def get_distance(t_total, t_hold):
    return t_hold * (t_total-t_hold)

def solve_square_eq(a,b,c):
    D = b*b - 4*a*c
    x1 = (-b + math.sqrt(D))/(2*a) 
    x2 = (-b - math.sqrt(D))/(2*a) 
    return (x1,x2)

def get_winning_range(t_total, s):
    a, b, c = -1, t_total, -s
    t_h1, t_h2 = solve_square_eq(a,b,c)
    return (t_h1, t_h2)


def main():

    f = open("input.txt", "r")
    times_string = f.readline().split(":")[1].strip()
    dists_string = f.readline().split(":")[1].strip()
    f.close()

    time = int(times_string.replace(" ", ""))
    dist = int(dists_string.replace(" ", ""))
   
    t_h1, t_h2 = get_winning_range(time, dist)
    n = get_nb_ways(time, dist, t_h1, t_h2)
    print(f"{time}-{dist} {t_h1}-{t_h2} {n}")


def test():
    print(get_distance(7,4))

main()
#test()
