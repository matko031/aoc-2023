import functools

types = {"H": 0, "1P": 1, "2P": 2, "3oK": 3, "FH": 4, "4oK": 5, "5oK": 6}
vals = {"A":14, "K":13, "Q":12, "J":11, "T":10, "9":9, "8":8, "7":7, "6":6, "5":5, "4":4, "3":3, "2": 2}


def main():
    f = open("input.txt", "r")
    hands = []
    for line in f.readlines():
        cards, bid = parse_line(line[:-1])
        cards_dict = get_hand(cards)
        hand_type = get_hand_type(cards_dict)
        hands.append((cards, hand_type, bid))
    f.close()

    compare_hands_key = functools.cmp_to_key(compare_hands_func)
    hands.sort(key=compare_hands_key)

    s = 0
    for i, hand in enumerate(hands):
        s += (i+1) * hand[2]
    print(s)



def parse_line(line):
    cards, bid = line.split(" ")
    bid = int(bid)
    return cards, bid

def get_hand(cards):
    cd = {}
    for c in cards:
        if c not in cd:
            cd[c] = 0
        cd[c] += 1
    return cd

def get_hand_type(hand):
    values = sorted(hand.values())

    if len(hand) == 5:
        return "H"

    elif len(hand) == 4:
        return "1P"

    if len(hand) == 3:
        if 3 in hand.values():
            return "3oK"

        elif sorted(hand.values()) == [1,2,2]:
            return "2P"

    elif len(hand) == 2:
        if 4 in hand.values():
            return "4oK"

        elif 3 in hand.values():
            return "FH"

    elif len(hand) == 1:
        return "5oK"

def compare_hands_func(h1, h2):
    c1, t1, b1 = h1
    c2, t2, b2 = h2
    v1, v2 = types[t1], types[t2]

    if v1 != v2:
        if v1 > v2:
            return 1
        else:
            return -1

    for i in range(5):
        print(c1,c2)
        v1, v2 = vals[c1[i]], vals[c2[i]],
        if v1 > v2:
            return 1
        elif v1 < v2:
            return -1

    return 0



main()
