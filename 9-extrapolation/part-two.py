def main():
    f = open("input.txt", "r")
    s = 0
    for line in f.readlines():
        data = parse_line(line[:-1])
        diffs = get_diffs(data)
        val = extrapolate(diffs)
        s += val
    f.close()
    print(s)


def parse_line(line):
    return list(map(int, line.strip().split(" ") ))

def get_diffs(data):
    diffs = [data]
    stop = False
    curr_diffs = data
    while not stop:
        stop = True
        new_diffs = []
        for i in range(1, len(curr_diffs)):
            new_val = curr_diffs[i]-curr_diffs[i-1]
            new_diffs.append(new_val)
            if new_val != 0:
                stop = False
        diffs.append(new_diffs)
        curr_diffs = new_diffs
    return diffs

"""
*5*  10  13  16  21  30  45
  *5*   3   3   5   9  15
   *-2*   0   2   4   6
      *2*   2   2   2
        *0*   0   0

2 - 0 = 2
"""

def extrapolate(diffs):
    new_val=0
    for i in range(2,len(diffs)+1):
        d = diffs[-i]
        old_val=new_val
        first_el = d[0]
        new_val = first_el - old_val
        #print(new_val, first_el, old_val)
    return new_val


main()
